# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]
- Moving dictionary MySQL,
- Cleaning up the code and formatting to PSR-2,
- Move key functions and classes to separate folder,
- Converting functions to modern PHP Object model,
- Visual interface (forms) of adding new data to dictionary,
- Fancy look,
- Authorisation,
- Put log into separate file,
- Add checkboxes into request for with selection what data to parse,
- Supporting NFL.com and other data sources.

## v0.1 - 2019-08-04
### Added
- Parsing Gamecast pages from ESPN.com
- Converting Game Leaders, Scoring Summary and Qureters score to EasyTables
- Displaying data in copy-and-paste format
- Output of log events
- Keeping translations in .ini files